/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package numerico_ode.problems;

import numerico_ode.interpolation.StateFunction;
import numerico_ode.interpolation.*;
import numerico_ode.methods.*;
import numerico_ode.ode.*;
import numerico_ode.tools.*;

/**
 *
 * @author paco
 */
public class LotkaVolterra implements InitialValueProblem {
    static private double a = 0.1;
    static private double b = 0.2;
    static private double c = 0.2;
    static private double d = 0.05;
    static private double f = 0.0; // Modificar para introducir pesca
    
    // Se “jugará” con initY para comprobar o refutar las conjeturas…   
    static private double initX = 0.25, initY = 0.10; 
        
    // ------------------
    // Implementation of InitialValueProblem
    // ------------------

    public double getInitialTime() { 
        return 0; 
    }
    
    public double[] getInitialState() { // x,vx, y,vy 
        return new double[] { initX, initY };
    } 
    
    public double[] getDerivative(double t, double[] x) {
        return new double[] { 
             (a-f)*x[0] - b*x[0]*x[1], 
            -(c+f)*x[1] + d*x[0]*x[1] 
        };
    }

     // ------------------
    // Methods for exploration
    // ------------------
    
    static public NumericalSolution checkConvergence(InitialValueProblem problem, double h, double maxTime, double tolerance, int maxIter) {
        FixedStepMethod method = new FixedStepRungeKutta4Method(problem,h);
        method.solve(maxTime);
        NumericalSolution fullStepSolution = method.getSolution();
        int iter = 0;
        do {
            h = h/2.0;
            System.out.println ("Trying with h/2 ="+h);
            method = new FixedStepRungeKutta4Method(problem,h);
            method.solve(maxTime);
            NumericalSolution halfStepSolution = method.getSolution();
            double error = FixedStepMethod.maxHalfStepError (fullStepSolution, halfStepSolution);
            if (error<tolerance) {
                System.out.println ("Tolerance reached at h="+h+", error = "+error);
                return halfStepSolution;
            }
            fullStepSolution = halfStepSolution;
            iter++;
        } while (iter<maxIter);
        return null;
    }
    
    // ------------------
    // Methods for detecting our event
    // ------------------
    
    static private double initVy = -(c+f)*initY + d*initX*initY;

    static private boolean crossingDetected (NumericalSolutionPoint fromPoint, NumericalSolutionPoint toPoint) {
        if (toPoint.getState(1)==initY) return true; // unlikely, but ...
        if ((fromPoint.getState(1) - initY) * (toPoint.getState(1) - initY) >= 0) return false;
        double vy = -(c+f)*toPoint.getState(1) + d*toPoint.getState(0)*toPoint.getState(1);
        return (vy*initVy>=0); // we want the crossing in one direction only
    }
    
    static private NumericalSolutionPoint computeCrossing (InitialValueProblem problem,
            NumericalSolutionPoint fromPoint, NumericalSolutionPoint toPoint, 
            double tolerance) {
        
        if (toPoint.getState(1)==initY) return toPoint; // unlikely, but ...

        StateFunction interpolator = new HermiteInterpolator(problem, fromPoint, toPoint);
        StateFunction zeroFunction = new StateFunction() {
            public double[] getState(double time) {
                double[] state = interpolator.getState(time);
                state[1] -= initY;
                return state;
            }
            public double getState(double time, int index) {
                double state = interpolator.getState(time, index);
                if (index == 1) {
                    state -= initY;
                }
                return state;
            }
        };
        double zeroAt = BisectionMethod.findZero (zeroFunction, fromPoint.getTime(), toPoint.getTime(), tolerance, 1);
        if (Double.isNaN(zeroAt)) {
            System.out.print ("Crossing through "+initY+" not found!!!");
            return null;
        }
        return new NumericalSolutionPoint (zeroAt, interpolator.getState(zeroAt));
    }
    
    static private double addToSum (NumericalSolutionPoint a, NumericalSolutionPoint b, int index) {
      double step = b.getTime()-a.getTime();
      return step*(b.getState(index)+a.getState(index))/2;
    }
    
    public static void main(String[] args) {
        double hStep = 0.01;
        double tolerance = 1.0e-8;
        double maxTime = 1000;

        InitialValueProblem problem = new LotkaVolterra();
        FixedStepMethod method;
        //method = new FixedStepEulerMethod(problem,hStep);
        //method = new FixedStepModifiedEulerMethod(problem,hStep);
        //method = new FixedStepPredictorCorrector4Method(problem,hStep);
        method = new FixedStepRungeKutta4Method(problem,hStep);
        //method = new AdaptiveStepPredictorCorrector4Method(problem,hStep, tolerance);
        //method = new AdaptiveStepRKFehlbergMethod(problem,hStep, tolerance);
        NumericalSolution solution=null;
        
        boolean explorationPhase = false;
        if (explorationPhase) { // First explorations
            solution = checkConvergence(problem, hStep, maxTime, tolerance, 30);
        }
        else { // Now, we look for the period...
            NumericalSolutionPoint previousPoint, currentPoint;
            previousPoint = currentPoint = method.getSolution().getLastPoint();
            int crossing = 0, maxCrossing = 10;
            double lastCrossingAt = 0;
            double xSum = 0;
            double ySum = 0;
            while (crossing < maxCrossing && currentPoint.getTime() < maxTime) {
                previousPoint = currentPoint;
                currentPoint = method.step();
                if (crossingDetected(previousPoint,currentPoint)) {
                    NumericalSolutionPoint crossingPoint = computeCrossing(problem,previousPoint, currentPoint, tolerance);
                    // Crossed 
                    double currentPeriod = crossingPoint.getTime() - lastCrossingAt;
                    crossing++;
                    lastCrossingAt = crossingPoint.getTime();
                    xSum += addToSum(previousPoint, crossingPoint,0);
                    ySum += addToSum(previousPoint, crossingPoint,1);
                    double xAverage = xSum/currentPeriod;
                    double yAverage = ySum/currentPeriod;
                    System.out.println("====================================");
                    System.out.println("Crossing: " + crossing);
                    System.out.println("Crossing time: " + crossingPoint.getTime());
                    System.out.println("Period : " + currentPeriod);
                    System.out.println("x Average : " + xAverage);
                    System.out.println("y Average : " + yAverage);
                    System.out.println("====================================");
                    xSum = addToSum(crossingPoint, currentPoint,0); 
                    ySum = addToSum(crossingPoint, currentPoint,1); 
                }
                else {
                    xSum += addToSum(previousPoint, currentPoint,0);
                    ySum += addToSum(previousPoint, currentPoint,1);
                }
            }
            previousPoint.println();
            currentPoint.println();
            solution = method.getSolution();
        }
        
        System.out.println ("Evaluations ="+method.getEvaluationCounter());

        DisplaySolution.timePlot(solution);
        DisplaySolution.statePlot(solution, 0, 1);
        if (method instanceof AdaptiveStepMethod) 
            DisplaySequence.plot(((AdaptiveStepMethod) method).getStepList());
    }
}
